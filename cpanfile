requires "App::Munner";
requires "Dancer";
requires "Plack";
requires 'Plack::Middleware::ReverseProxy';
requires 'Plack::Middleware::NoMultipleSlashes';
requires "Starman";
requires "YAML";
