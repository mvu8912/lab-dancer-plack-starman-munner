#!/bin/sh
### BEGIN INIT INFO
# Provides: Foo
# Required-Start: $network
# Required-Stop: $network
# Default-Start: 2 3 4 5
# Default-Stop:  0 1 6
# Description: Foo Website
### END INIT INFO
if [ $1 = "start" ]; then
    CMD=duck
else
    CMD=$1
fi

cd /home/foo/project

/usr/local/bin/perlbrew exec --with 5.16.3 --quiet -- carton exec -- munner $CMD -g initd

exit 0
