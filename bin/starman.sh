#!/bin/sh

if [ "$PLACK_ENV" = "" ]; then
    echo "Missing PLACK_ENV global environment variable"
    exit 2
fi

if [ "$NUMBER_OF_WORKERS" = "" ]; then
    echo "Missing NUMBER_OF_WORKERS global environment variable"
    exit 2
fi

if [ "$PM_MAX_REQUESTS" = "" ]; then
    PM_MAX_REQUESTS=200
fi

if [ "$BASE_DIR" = "" ]; then
    BASE_DIR=.
fi

HOST=127.0.0.1

if [ "$PORT" = "" ]; then
    PORT=5000
fi

DANCER_APP=$BASE_DIR/bin/app.pl

PLACK_SERVER=Starman

ERROR_LOG=$BASE_DIR/error.log
ACCESS_LOG=$BASE_DIR/production.log

cd $BASE_DIR

echo $DANCER_APP

exec starman
     --user         $USER              \
     --env          $PLACK_ENV         \
     --host         $HOST              \
     --port         $PORT              \
     --workers      $NUMBER_OF_WORKERS \
     --max-requests $PM_MAX_REQUESTS   \
     --error-log    $ERROR_LOG         \
     --access-log   $ACCESS_LOG        \
